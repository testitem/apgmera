import random
import string
import os
import time
import shutil
import subprocess
import requests
import json
import hashlib
from concurrent.futures import ThreadPoolExecutor
from collections import Counter

def read_strip(filename):
    with open(filename) as f:
        x = f.read().strip()
    return x

def json_dump(x, filename):
    with open(filename, 'w') as f:
        json.dump(x, f)

def json_load(filename):
    with open(filename) as f:
        x = json.load(f)
    return x

CE_KEY = read_strip("scripts/ce_authkey")



ENDPOINT = "https://api.charityengine.services/remotejobs/v2/"
THREADS = 4

nJobsPerHaul = 50
nJobSize = 10 ** 7

assert nJobSize % 10 == 0

# do not send out more than this much work every half hour
maxWorkUnits = 10000

currentHaulsInProgress = 0
currentJobsDone = 0

dct = {}
jobsymid = {}

CATAGOLUE = "https://catagolue.hatsya.com"

symstatus = {}

reslinks = {}

def getobjects(sym):
    try:
        r = requests.get(
            f"https://catagolue.hatsya.com/census/b3s23/{sym}", timeout=600
        ).content.decode("utf8")
        r = r.replace("&#8201;", "")
        pos = r.find("containing")
        v = r[pos : pos + 100]
        return int(v.split()[1])
    except:
        return -1


def catagolueRequest(payload, endpoint="/payosha256"):
    headers = {
        "Connection": "close",
        "Content-Type": "text/plain",
        "User-Agent": "Anaconda-urllib/2.7",
    }
    try:
        r = requests.post(
            CATAGOLUE + endpoint, data=payload, headers=headers, timeout=600
        )
    except requests.Timeout:
        log("catagolue request timeout")
        return None
    if r.status_code != 200:
        return None
    return r.content.decode("utf8")


def authenticate(payoshakey, operation):
    payload = "payosha256:get_token:%s:%s" % (payoshakey, operation)
    resp = catagolueRequest(payload)
    if resp is None:
        return None
    if len(resp) == 0:
        return None
    target = token = ""
    for i in resp.split("\n"):
        if "good" in i.split(":"):
            _, _, target, token = i.split(":")
    if len(token) == 0:
        return None
    for nonce in range(244823040):
        prehash = "%s:%d" % (token, nonce)
        posthash = hashlib.sha256(prehash.encode("utf8")).hexdigest()
        if posthash < target:
            return "payosha256:pay_token:%s\n" % prehash
    return None


def submit(fname):
    try:
        payoshakey = read_strip("scripts/payoshakey")
        authstring = authenticate(payoshakey, "post_apgsearch_haul")
        if authstring is None:
            return None
        res = "%s\n@VERSION charityengine.com\n" % authstring
        obj = linenum = 0

        with open(fname) as f:
            for i in f:
                linenum += 1
                if linenum <= 2:
                    continue
                i = i.strip()
                res += i + "\n"
                if linenum == 8:
                    _, obj = i.split()

        x = catagolueRequest(res, "/apgsearch")
        if x is None:
            return None
        if len(x) < 90:
            print("\033[32;1m%s\033[0m" % x)
            return int(obj)
        else:
            print("\033[31;1m%s\033[0m" % x)
            return 0
    except:
        return None


def log(s: str):
    with open("log.txt", "a") as l:
        print(f"{time.asctime()}    {currentHaulsInProgress} {currentJobsDone} {s}")
        print(
            f"{time.asctime()}    {currentHaulsInProgress} {currentJobsDone} {s}",
            file=l,
        )


def remap(fun, data, threads, delay=0):
    res = [None] * len(data)
    fs = [None] * len(data)
    with ThreadPoolExecutor(max_workers=threads) as executor:
        de = 0
        while True:
            idxs = [i for (i, x) in enumerate(res) if (x is None)]
            log('remap: %d indices remaining' % len(idxs))
            if len(idxs) == 0:
                break
            time.sleep(de)
            de += 60
            for i in idxs:
                time.sleep(delay)
                fs[i] = executor.submit(fun, data[i])
            for i in idxs:
                res[i] = fs[i].result()
    return res


def syncStatus():
    log("running syncStatus()")
    global symstatus
    r = "C1 D8_1 D8_4 C4_1 C4_4 D4_x1 D4_x4 D4_+1 D4_+2 D4_+4 C2_1 C2_2 C2_4".split()
    s = remap(getobjects, r, 16)
    for a, b in zip(r, s):
        if b != -1:
            symstatus[a] = b
    log("completed syncStatus()")


def onLoad():
    os.makedirs("scripts", exist_ok=True)
    os.makedirs("scripts/logs2", exist_ok=True)
    # os.makedirs("scripts/archives", exist_ok=True)
    # os.makedirs("scripts/results", exist_ok=True)
    hauls = os.listdir("scripts/logs2")
    global currentHaulsInProgress
    currentHaulsInProgress = len(hauls)
    global currentJobsDone
    currentJobsDone = 0
    # for haul in hauls:
    #     currentJobsDone += len(os.listdir(f"scripts/logs2/{haul}"))
    global dct
    global jobsymid
    global reslinks
    try:
        dct = json_load("work")
    except FileNotFoundError:
        pass
    json_dump(dct, "work")
    try:
        jobsymid = json_load("work_sym")
    except FileNotFoundError:
        pass
    json_dump(jobsymid, "work_sym")
    try:
        reslinks = json_load("work_reslinks")
    except FileNotFoundError:
        pass
    json_dump(reslinks, "work_reslinks")
    syncStatus()
    for haul in hauls:
        if haul in reslinks:
            currentJobsDone += len(reslinks[haul])
    # f = input("sync? ")
    # f = 0
    # if f:
    # syncWork()


def postApi(tup):
    endpoint, j = tup

    try:
        res = requests.post(ENDPOINT + endpoint, json=j, timeout=600).json()
        if ("success" in res):
            return res
        else:
            log("postApi: no success code: %s" % res)
    except requests.Timeout:
        log(f"postApi: timeout {endpoint}")
    except Exception as e:
        log("postApi: exception %s" % repr(e))


def download(tup):
    fname, url = tup
    while 1:
        try:
            with requests.get(url, stream=True, timeout=600) as r:
                with open(fname, "wb") as f:
                    shutil.copyfileobj(r.raw, f)
            return True
        except:
            log(f"timeout download")
            # open(fname, "wb")
            if os.path.exists(fname):
                os.remove(fname)
            time.sleep(60)
            pass


def cleanWork():
    json_dump(dct, "work")
    olen = len(read_strip("work"))
    hauls = set(os.listdir("scripts/logs2"))
    for haul in list(dct.keys()):
        if haul not in hauls:
            del dct[haul]
    json_dump(dct, "work")
    nlen = len(read_strip("work"))
    log(
        "clean work: %d -> %dKB, freed %dKB"
        % (olen // 1024, nlen // 1024, (olen - nlen) // 1024)
    )
    json_dump(reslinks, "work_reslinks")
    olen = len(read_strip("work_reslinks"))
    for haul in list(reslinks.keys()):
        if haul not in hauls:
            del reslinks[haul]
    json_dump(reslinks, "work_reslinks")
    nlen = len(read_strip("work_reslinks"))
    log(
        "clean resl: %d -> %dKB, freed %dKB"
        % (olen // 1024, nlen // 1024, (olen - nlen) // 1024)
    )


def _makeWork2(workList):
    to = []
    for haulRoot, b in workList:
        symid = jobsymid[haulRoot]
        cmd = f" -n {nJobSize} -b {b} -s {haulRoot} -y {symid}"
        to += [
            (
                "job-create",
                {
                    "authenticator": CE_KEY,
                    "app": "charityengine:apgluxe",
                    "commandLine": cmd,
                    "hours": 2,
                },
            )
        ]
    global dct
    cnt = 1024
    for i in range(0, len(to), cnt):
        print("%d / %d" % (i, len(to)))
        to2 = to[i : i + cnt]
        x = remap(postApi, to2, THREADS * 16)
        for w, ii in zip(workList[i : i + cnt], x):
            root, b = w
            if root not in dct:
                dct[root] = {}
            if "ids" not in ii:
                ii = str(ii)
                log("unhelpful response: %s" % ii)
                if 'scheduled' in ii.lower():
                    log('cancelling...')
                    return
            else:
                dct[root][str(b)] = ii["ids"][0]
        json_dump(dct, "work")

def _makeWork(workList):
    _makeWork2(workList)
    if len(workList) > 10:
        log('cleaning...')
        cleanWork()


def pollResults():
    tt = time.time()
    hauls = os.listdir("scripts/logs2")
    work = []
    global reslinks

    pathologicals = []

    for haul in hauls:
        # s = os.listdir(f"scripts/logs2/{haul}")
        for i in range(nJobsPerHaul):
            b = str(i * nJobSize)
            if haul not in reslinks or b not in reslinks[haul]:
                if haul not in dct:
                    log('Deleting directory %s' % haul)
                    os.rmdir(os.path.join("scripts/logs2", haul))
                    break
                elif b not in dct[haul]:
                    pathologicals.append((haul, b))
                else:
                    work.append((haul, b, dct[haul][b]))
    s = [k for i, j, k in work]

    n_items = len(s)
    log('Polling %d work units...' % n_items)

    stat = {}
    to = []
    for i in range(0, n_items, 300):
        s2 = s[i : i + 300]
        to += [("jobs-status", {"authenticator": CE_KEY, "keys": s2})]
    # print(len(to))
    # print(to[0])
    # print(postApi(to[0]))
    # exit(0)
    x = remap(postApi, to, THREADS * 4, 0.1)
    for pp, res in zip(to, x):
        for i in pp[1]["keys"]:
            jobs = res.get("jobs", [])
            stat[i] = jobs.get(i, {}) if (len(jobs) > 0) else {}
    fls = []
    statuses = []

    for i, j, k in work:
        wu_status = stat[k].get("vmStatus", "lost")
        statuses.append(wu_status)
        if wu_status == "completed":
            canonicalOutput = stat[k]["states"]["default"]["outputFiles"]
            # print(k, canonicalOutput)
            if len(canonicalOutput) == 1:
                if i not in reslinks:
                    reslinks[i] = {}
                reslinks[i][j] = canonicalOutput[0]["url"]
                # fls += [(f"scripts/logs2/{i}/{j}", canonicalOutput[0]["url"])]
            else:
                log(f"WeirdChamp output file doesn't exist: {i} {j} {dct[i][j]}")
                pathologicals.append((i, j))
        elif wu_status == "lost":
            pathologicals.append((i, j))

    cs = Counter(statuses)
    log(str(cs))

    if (len(pathologicals) > maxWorkUnits):
        log("Reducing from %d to %d" % (len(pathologicals), maxWorkUnits))
        pathologicals = pathologicals[:maxWorkUnits]

    log("Retrying %d jobs" % len(pathologicals))
    _makeWork(pathologicals)
    json_dump(reslinks, "work_reslinks")
    # remap(download, fls, THREADS * 8)
    global currentJobsDone
    pre = currentJobsDone
    currentJobsDone = 0
    for haul in hauls:
        if haul in reslinks:
            currentJobsDone += len(reslinks[haul])
    ut = time.time() - tt
    log(
        "polled %d got %d in %d:%.2f"
        % (len(work), currentJobsDone - pre, ut // 60, ut % 60)
    )

    return cs


def gensym():
    with open("symdistrib") as f:
        li = [i.strip() for i in f]
    s = random.choice(li).split()
    f = min((symstatus[i], i) for i in s)[1]
    symstatus[f] += 2 * 10 ** 8 * nJobsPerHaul
    return f


def makeWork(hauls: int):
    global jobsymid
    tt = time.time()
    work = []
    for _ in range(hauls):
        haulRoot = "c_" + "".join(
            random.choice(string.ascii_letters + string.digits) for i in range(12)
        )
        jobsymid[haulRoot] = gensym()
        outputdirRoot = f"scripts/logs2/{haulRoot}"
        os.mkdir(outputdirRoot)
        for i in range(nJobsPerHaul):
            b = i * nJobSize
            work += [(haulRoot, b)]
    json_dump(jobsymid, "work_sym")
    _makeWork(work)
    global currentHaulsInProgress
    currentHaulsInProgress += hauls
    ut = time.time() - tt
    log("sent out %d in %d:%.2f" % (hauls, ut // 60, ut % 60))


# maintainence function
def extendCurrent(force=False):
    onLoad()

    with open("work") as f:
        with open(f"work{int(time.time())}", "w") as g:
            g.write(f.read())

    tt = time.time()
    hauls = os.listdir("scripts/logs2")
    work = []
    for haul in hauls:
        s = os.listdir(f"scripts/logs2/{haul}")
        for i in range(nJobsPerHaul):
            b = i * nJobSize
            if str(b) not in s and (
                force or haul not in dct or str(b) not in dct[haul]
            ):
                work += [(haul, b)]
    _makeWork(work)
    ut = time.time() - tt
    log("sent out %d WORK UNITS in %d:%.2f" % (len(work), ut // 60, ut % 60))


# maintainence function
def resend(haul, b):
    fname = f"scripts/logs2/{haul}/{b}"
    # open(fname, "wb")
    os.remove(fname)
    if haul in reslinks and b in reslinks[haul]:
        del reslinks[haul][b]
    _makeWork([(haul, b)])

def ppcs(hauls):
    tt = time.time()
    fls = []
    for haul in hauls:
        if haul not in reslinks:
            continue
        _s = len(reslinks[haul])
        if _s != nJobsPerHaul:
            continue
        for j in reslinks[haul]:
            fls += [(f"scripts/logs2/{haul}/{j}", reslinks[haul][j])]
    print("...downloading",len(fls))
    remap(download, fls, THREADS * 8)
    ut = time.time() - tt
    log("downloaded %d workunits in %d:%.2f" % (len(fls), ut // 60, ut % 60))

def mergeSpecificHaul(haul):
    # s = os.listdir(f"scripts/logs2/{haul}")
    if haul not in reslinks:
        return 0
    _s = len(reslinks[haul])
    if _s != nJobsPerHaul:
        return 0
    
    s = os.listdir(f"scripts/logs2/{haul}")
    if len(s) != nJobsPerHaul:
        print(s)
        log("something is wrong")
        return 0
    print(haul)
    census = {}
    samples = {}
    numsoups = 0
    numobjects = 0
    failed = 0
    for i in s:
        fname = f"scripts/logs2/{haul}/{i}"
        with open(fname) as fi:
            try:
                state = ""
                thisoup = 0
                for line in fi:
                    line = line.strip()
                    if not line:
                        continue
                    if line == "testing":
                        continue
                    if line[0] == "@":
                        if line == "@CENSUS TABLE":
                            state = "census"
                        elif line == "@SAMPLE_SOUPIDS":
                            state = "soups"
                        else:
                            a, b = line.split()
                            dct[a] = b
                            if a == "@NUM_SOUPS":
                                thisoup = int(b)
                                numsoups += int(b)
                            if a == "@NUM_OBJECTS":
                                numobjects += int(b)
                    elif state == "census":
                        a, b = line.split()
                        if a not in census:
                            census[a] = 0
                        census[a] += int(b)
                    elif state == "soups":
                        z = line.split()
                        if z[0] not in samples:
                            samples[z[0]] = []
                        samples[z[0]] += list(map(int, z[1:]))
                    else:
                        log(f"WeirdChamp incorrectly formatted: {haul} {i} {dct[haul][i]}")
                        resend(haul, i)
                        return 0
                        # raise ValueError
                if state == "" or thisoup != nJobSize:
                    log(f"WeirdChamp incorrectly formatted: {haul} {i} {dct[haul][i]}")
                    resend(haul, i)
                    return 0
            except Exception as e:
                log(f"WeirdChamp {haul} {i} {dct[haul][i]} error {str(e)}")
                resend(haul, i)
                return 0
    assert numsoups == nJobSize * nJobsPerHaul
    dct["@NUM_SOUPS"] = numsoups
    dct["@NUM_OBJECTS"] = numobjects
    with open(f"scripts/logs2/{haul}/log.txt", "w") as f:
        print("testing", file=f)
        for i in [
            "@VERSION",
            "@MD5",
            "@ROOT",
            "@RULE",
            "@SYMMETRY",
            "@NUM_SOUPS",
            "@NUM_OBJECTS",
        ]:
            print(i, dct[i], file=f)
        print(file=f)
        print("@CENSUS TABLE", file=f)
        pp = sorted([(census[i], i) for i in census], reverse=1)
        for cnt, obj in pp:
            print(obj, cnt, file=f)
        print(file=f)
        print("@SAMPLE_SOUPIDS", file=f)
        for cnt, obj in pp:
            s2 = sorted(samples[obj])[:10]
            print(obj, *s2, file=f)
    return 1


def mergeUploadCleanup(hauls):
    ppcs(hauls)
    tt = time.time()
    fp = []
    for haul in hauls:
        try:
            if mergeSpecificHaul(haul):
                fp += [f"scripts/logs2/{haul}/log.txt"]
        except:
            log(f"jank {haul} [ this should not happen ]")
    remap(submit, fp, THREADS * 16)
    global currentHaulsInProgress
    global currentJobsDone
    for haul in hauls:
        s = os.listdir(f"scripts/logs2/{haul}")
        if "log.txt" in s:
            # shutil.copyfile(
            #     f"scripts/logs2/{haul}/log.txt", f"scripts/results/{haul}.txt"
            # )
            f = haul[2]
            # os.system(f"zip -r scripts/archives/{f}.zip scripts/results/{haul}.txt")
            shutil.rmtree(f"scripts/logs2/{haul}")
            currentHaulsInProgress -= 1
            currentJobsDone -= nJobsPerHaul
    ut = time.time() - tt
    log("uploaded %d in %d:%.2f" % (len(fp), ut // 60, ut % 60))


# maintainence function
def redownload(haul, b):
    work = [(haul, str(b), dct[haul][str(b)])]
    s = [dct[haul][str(b)]]
    print(s)
    stat = {}
    to = [("jobs-status", {"authenticator": CE_KEY, "keys": s})]
    x = remap(postApi, to, THREADS * 12)
    for pp, res in zip(to, x):
        for i in pp[1]["keys"]:
            stat[i] = res["jobs"][i]
    for i, j, k in work:
        assert stat[k]["vmStatus"] == "completed"
        canonicalOutput = stat[k]["states"]["default"]["outputFiles"]
        assert len(canonicalOutput) == 1
        print(canonicalOutput[0]["url"])
        download((f"scripts/logs2/{i}/{j}", canonicalOutput[0]["url"]))


def mainloop():
    log("running mainloop()")
    cs = pollResults()
    hauls = os.listdir("scripts/logs2")
    mergeUploadCleanup(hauls)
    # for i in range(0, len(hauls), 1200):
    #     mergeUploadCleanup(hauls[i:i+1200])
    # exit(0)
    try:
        nJobsRunningMin = int(read_strip("nJobsRunningMin"))
    except:
        nJobsRunningMin = 12000
        pass
    unfinished = nJobsPerHaul * currentHaulsInProgress - currentJobsDone

    # hard limit on the number of concurrent work units in Charity Engine:
    bound1 = nJobsRunningMin - unfinished

    # avoid creating lots of jobs when there are already plenty enqueued:
    bound2 = maxWorkUnits - cs.get('provisioning', 0)
    to_send = min(bound1, bound2) // nJobsPerHaul

    if to_send > 0:
        makeWork(to_send)

    log("completed mainloop()")


def seekWu(wuid):
    for haul in dct:
        for i in dct[haul]:
            if dct[haul][i] == wuid:
                print(haul, i)
                return


if __name__ == "__main__":
    onLoad()
    print(symstatus)
    # extendCurrent()
    mainloop()
    cur = time.time()
    it = 0
    while 1:
        cur = time.time()
        it += 1
        if it % 10 == 0:
            syncStatus()
        mainloop()
        z = cur + 900.0 - time.time()
        if z > 0:
            time.sleep(z)
