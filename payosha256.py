import requests
import hashlib

CATAGOLUE = "https://catagolue.hatsya.com"


def catagolueRequest(payload, endpoint="/payosha256"):
    headers = {
        "Connection": "close",
        "Content-Type": "text/plain",
        "User-Agent": "Anaconda-urllib/2.7",
    }
    r = requests.post(CATAGOLUE + endpoint, data=payload, headers=headers)
    if r.status_code != 200:
        raise Exception("Bad status: %d" % r.status_code)
    return r.content


def authenticate(payoshakey, operation):
    payload = "payosha256:get_token:%s:%s" % (payoshakey, operation)
    resp = catagolueRequest(payload).decode("utf8")
    if len(resp) == 0:
        raise Exception("No response!")
    target = token = ""
    for i in resp.split("\n"):
        if "good" in i.split(":"):
            _, _, target, token = i.split(":")
    if len(token) == 0:
        raise Exception("Invalid response from payosha256.")
    for nonce in range(244823040):
        prehash = "%s:%d" % (token, nonce)
        posthash = hashlib.sha256(prehash.encode("utf8")).hexdigest()
        if posthash < target:
            return "payosha256:pay_token:%s\n" % prehash
    raise Exception("wtf")


def submit(file):
    try:
        payoshakey = open("scripts/payoshakey").read().strip()
        authstring = authenticate(payoshakey, "post_apgsearch_haul")
        res = "%s\n@VERSION charityengine.com\n" % authstring
        obj = linenum = 0
        for i in file:
            linenum += 1
            if linenum <= 2:
                continue
            i = i.strip()
            res += i + "\n"
            if linenum == 8:
                _, obj = i.split()
        x = catagolueRequest(res, "/apgsearch").decode("utf8")
        if len(x) < 90:
            print("\033[32;1m%s\033[0m" % x)
            return int(obj)
        else:
            print("\033[31;1m%s\033[0m" % x)
            return 0
    except:
        return 0


print(submit(open("scripts/results/c_002LhoDfKq2W.txt")))
