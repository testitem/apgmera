#pragma once

#include <iomanip>
#include <stdio.h>

#include <atomic>
#include <thread>
#include <chrono>
#include <random>

#ifdef USE_OPEN_MP
#include <omp.h>
#endif

#ifndef _WIN32
#include <sys/select.h>
#include <termios.h>
// determine whether there's a keystroke waiting
int keyWaiting() {
    struct timeval tv;
    fd_set fds;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); // STDIN_FILENO is 0

    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);

    return FD_ISSET(STDIN_FILENO, &fds);
}
#endif

void populateLuts() {

    apg::best_instruction_set();
    std::vector<apg::bitworld> vbw = apg::hashsoup("", SYMMETRY);

    apg::lifetree<uint32_t, BITPLANES> lt(LIFETREE_MEM);
    apg::pattern pat(&lt, lt.fromplanes(vbw), RULESTRING);

    pat.advance(0, 0, 8);

}

void partialBalancedSearch(std::vector<uint64_t> *vec, std::string seed, SoupSearcher *localSoup,
                            std::atomic<bool> *running, std::atomic<uint64_t> *idx, std::atomic<uint64_t> *ts) {

    uint64_t maxidx = vec->size();

    apg::lifetree<uint32_t, BITPLANES> lt(LIFETREE_MEM);
    apg::base_classifier<BITPLANES> cfier(&lt, RULESTRING);

    while (*running) {

        uint64_t curridx = ((*idx)++); // atomic increment
        if (curridx >= maxidx) { break; }

        uint64_t suffix = (*vec)[curridx];

        localSoup->censusSoup(seed, strConcat(suffix), cfier);
        (*ts)++;

    }
}

struct CpuSearcher {

    void pump(const std::string& seed, uint64_t j, std::vector<uint64_t> &vec) {

        (void) seed;

        for (uint64_t i = 0; i < 100000; i++) {
            vec.push_back(j * 100000 + i);
        }
    }

};

std::vector<uint64_t> narrow(const std::vector<uint64_t>& orig, uint64_t lb, uint64_t ub) {

    std::vector<uint64_t> narrowed;
    for (uint64_t x : orig) {
        if (lb <= x && x < ub) {
            narrowed.push_back(x);
        }
    }
    return narrowed;

}

std::string retrieveSeed(apg::lifetree<uint32_t, BITPLANES> &lt) {
    std::string seed;
    bool readingrle = false;
    std::string stdin_line;
    while (std::getline(std::cin, stdin_line)) {
        if (stdin_line.empty()) {
            continue;
        }
        if (readingrle) {
            seed += '-';
            seed += stdin_line;
        }
        if ((stdin_line[0] == 'x') && !readingrle) {
            readingrle = true;

            // if ((stdin_line[1] == 'p') || (stdin_line[1] == 'q') || (stdin_line[1] == 's')) {
            //     apg::pattern pat(&lt, stdin_line, RULESTRING);
            //     std::ostringstream ss;
            //     pat.write_rle(ss);
            //     std::string rle_string = ss.str();
            //     rle_string = rle_string.substr(rle_string.find("x"));
            //     rle_string = rle_string.substr(rle_string.find("\n"));
            //     rle_string = rle_string.substr(0, rle_string.find("!") + 1);
            //     std::replace(rle_string.begin(), rle_string.end(), '\n', '-');
            //     seed = rle_string;
            //     break;
            // }
        }
        if (stdin_line.find('!') != std::string::npos) {
            break;
        }
    }
    return seed;
}

std::string n_glider_gen(std::mt19937& rng) {
    static bool aray[75][75];
    const int BBOX = 70;
    static const int glide[16] = {143, 346, 107, 286, 302, 115, 167, 370,
                       482, 181, 428, 241, 233, 412, 458, 157};
    const int gc = 5;
    memset(aray, 0, sizeof aray);
    int mj = BBOX;
    for (int i = 0; i < gc; i++) {
      int dx = rng() % 40, dy = rng() % 40, id = rng() % 16;
      switch (id / 4) {
      case 0:
        dx += 25;
        dy += 25;
        break;
      case 1:
        dx += 25;
        break;
      case 2:
        break;
      case 3:
        dy += 25;
        break;
      default:
        break;
      }
      bool f = false;
      for (int i = 0; i < 49; i++)
        if (0 <= dx + i / 7 - 2 && dx + i / 7 - 2 <= BBOX && 0 <= dy + i % 7 - 2 && dy + i % 7 - 2 <= BBOX &&
            aray[dx + i / 7 - 2][dy + i % 7 - 2]) {
          f = true;
          break;
        }
      if (f)
        continue;
      mj = std::min(mj, dy);
      for (int i = 0; i < 9; i++){
      //     if(aray[dx+i/3][dy+i%3]){
      //         std::cout<<"yeet"<<std::endl;
      //         exit(0);
      //     }
        aray[dx + i / 3][dy + i % 3] |= (bool)(glide[id] & (1 << i));
      }
    }
    std::string rle;
    for (int i = 0; i < 70; i++) {
      if (i)
        rle += '$';
      int cnt = 0, cv = 2;
      for (int j = mj; j < 70; j++) {
        if (aray[i][j] != cv) {
          if (cv != 2) {
            if (cnt != 1)
              rle += std::to_string(cnt);
            rle += cv ? 'o' : 'b';
          }
          cnt = 1;
          cv = aray[i][j];
        } else
          cnt++;
      }
      if (cv)
        rle += (std::to_string(cnt) + (cv ? 'o' : 'b'));
    }
    while (rle.back() == '$')
      rle.pop_back();
    std::string srle;
    int cnt = 0;
    for (char c : rle) {
      if (c == '$') {
        if (srle.length())
          cnt++;
      } else {
        if (cnt) {
          if (cnt > 1)
            srle += std::to_string(cnt);
          srle += '$';
        }
        cnt = 0;
        srle += c;
      }
    }
    srle += '!';
    return '-' + srle;
}

void perpetualSearch(uint64_t soupsPerHaul, int numThreads, bool interactive, const std::string& payoshaKey, const std::string& seed,
                        int unicount, int local_log, std::atomic<bool> &running, bool testing, uint64_t start) {
    /*
    Unifies several similar functions into one simpler one.
    */

    // #ifndef _WIN32
    // struct termios ttystate;

    // if (interactive) {
    //     // turn on non-blocking reads
    //     tcgetattr(STDIN_FILENO, &ttystate);
    //     ttystate.c_lflag &= ~ICANON;
    //     ttystate.c_cc[VMIN] = 1;
    //     tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
    // }
    // #endif

    // uint64_t soupsCompletedSinceStart = 0;
    uint64_t soupsCompletedBeforeLastMessage = 0;

    SoupSearcher globalSoup;
    globalSoup.tilesProcessed = 0;
    apg::lifetree<uint32_t, BITPLANES> lt(LIFETREE_MEM);
    apg::base_classifier<BITPLANES> cfier(&lt, RULESTRING);

    populateLuts();

    // std::cout << "Running " << soupsPerHaul << " soups per haul:" << std::endl;

    #ifdef USING_GPU
    uint64_t epoch_size = 1000000;
    apg::GpuSearcher gs(0, unicount, SYMMETRY);
    #else
    uint64_t epoch_size = 100000;
    CpuSearcher gs; (void) unicount;
    #endif

    std::vector<uint64_t> vec;
    // if (numThreads != 0) { gs.pump(seed, 0, vec); }

    auto timeOfStart = std::chrono::system_clock::now();
    auto timeOfLastMessage = timeOfStart;
    auto timeOfLastTimerCheck = timeOfStart;

    uint64_t maxcount = soupsPerHaul;

    uint64_t lb = 0, soupsCompletedSinceStart = 0;

    // std::mt19937 rng(42);

    // for(char c:seed + std::to_string(start)) {
    //     rng();
    //     rng();
    //     rng();
    //     rng = std::mt19937(rng() + c);
    // }

    for(uint64_t i = 0; i < maxcount; i += 10000) {
        for(uint64_t j = i; j < i + 10000; j++) {
            std::string suffix;
            // if(SYMMETRY.find("synth") != std::string::npos) {
            //     suffix = n_glider_gen(rng);
            //     if(suffix.empty()) {
            //         i = maxcount;
            //         break;
            //     }
            // }
            // else
                suffix = strConcat(j + start);
            globalSoup.censusSoup(seed, suffix, cfier);
            soupsCompletedSinceStart += 1;
        }
        std::ofstream progress("progress.txt");
        progress << std::min(1.0, 1.0 * (i + 10000) / maxcount) << std::endl;
        progress.close();
    }

    // while (maxcount--) {

        // if (numThreads == 0) {
// #ifdef STDIN_SYM
//            std::string suffix = retrieveSeed(&running, lt);
// #else
        //    std::string suffix = strConcat(soupsCompletedSinceStart + start);
// #endif

            // globalSoup.censusSoup(seed, suffix, cfier);
            // soupsCompletedSinceStart += 1;
        // } 
        // else {
        //     std::vector<SoupSearcher> localSoups(numThreads, SoupSearcher(&globalSoup));
        //     std::vector<std::thread> lsthreads(numThreads);

        //     std::atomic<uint64_t> idx(0);
        //     std::atomic<uint64_t> ts(0);

        //     auto nvec = narrow(vec, lb, maxcount);

        //     vec.clear();

        //     for (int j = 0; j < numThreads; j++) {
        //         lsthreads[j] = std::thread(partialBalancedSearch, &(nvec), seed, &(localSoups[j]), &running, &idx, &ts);
        //     }

        //     uint64_t newi = soupsCompletedSinceStart;
        //     lb = ((newi / epoch_size) + 1) * epoch_size;

        //     do {
        //         newi = ((newi / epoch_size) + 1) * epoch_size;
        //         if (newi < maxcount) {
        //             gs.pump(seed, newi / epoch_size, vec);
        //         } else {
        //             newi = maxcount;
        //         }
        //     } while ((newi < maxcount) && (vec.size() < 5000) && (running) && (ts < nvec.size()));

        //     for (int j = 0; j < numThreads; j++) {
        //         lsthreads[j].join();
        //         globalSoup.aggregate(&(localSoups[j].census), &(localSoups[j].alloccur));
        //     }

        //     if (running || (ts == nvec.size())) {
        //         soupsCompletedSinceStart = newi;
        //     } else {
        //         double diff = newi - soupsCompletedSinceStart;
        //         uint64_t estim = (diff * ts) / nvec.size();
        //         soupsCompletedSinceStart += estim;
        //     }
        // }

        // auto now = std::chrono::system_clock::now();
        // double secondsSinceLastMessage = 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(now - timeOfLastMessage).count();
        // double secondsSinceLastTimerCheck = 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(now - timeOfLastTimerCheck).count();
        // double secondsSinceStart = 0.001 * std::chrono::duration_cast<std::chrono::milliseconds>(now - timeOfStart).count();

        // timeOfLastTimerCheck = now;

        // bool finished = (!running) || ((soupsCompletedSinceStart == maxcount) && vec.empty());
        // uint64_t soupsCompletedSinceLastMessage = soupsCompletedSinceStart - soupsCompletedBeforeLastMessage;

        // if (finished || (secondsSinceLastMessage >= 10.0) || ((secondsSinceLastTimerCheck >= 1.0) && (soupsCompletedSinceLastMessage == 1)) || (soupsCompletedSinceLastMessage >= 1000000)) {
        //     std::cout << RULESTRING << "/" << SYMMETRY << ": " << soupsCompletedSinceStart << " soups completed ("
        //               << std::fixed << std::setprecision(3) << (soupsCompletedSinceLastMessage / secondsSinceLastMessage) << " soups/second current, "
        //               << (soupsCompletedSinceStart / secondsSinceStart) << " overall)." << std::endl;

        //     soupsCompletedBeforeLastMessage = soupsCompletedSinceStart;
        //     timeOfLastMessage = now;

        //     #ifndef _WIN32
        //     if (interactive && keyWaiting()) {
        //         char c = fgetc(stdin);
        //         if ((c == 'q') || (c == 'Q')) { running = false; finished = true; }
        //     }
        //     #endif
        // }

        // if (finished) {
        //     std::cout << "----------------------------------------------------------------------" << std::endl;
        //     std::cout << soupsCompletedSinceStart << " soups completed." << std::endl;
        //     std::cout << "Attempting to contact Catagolue..." << std::endl;
        //     std::string payoshaResponse = globalSoup.submitResults(payoshaKey, seed, soupsCompletedSinceStart, local_log, testing);

        //     if (payoshaResponse.length() == 0) {
        //         std::cout << "Connection was unsuccessful." << std::endl;
        //     } else {
        //         std::cout << "Connection was successful." << std::endl;
        //         break;
        //     }

        //     if (running) {
        //         std::cout << "Continuing search..." << std::endl;
        //         vec.clear();
        //         if (numThreads != 0) { gs.pump(seed, soupsCompletedSinceStart / epoch_size, vec); }
        //         maxcount += soupsPerHaul;
        //     }
        // }
    // }
    
    globalSoup.submitResults(payoshaKey, seed, soupsCompletedSinceStart, local_log, testing);

    // #ifndef _WIN32
    // if (interactive) {
    //     // turn on blocking reads
    //     tcgetattr(STDIN_FILENO, &ttystate);
    //     ttystate.c_lflag |= ICANON;
    //     tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
    // }
    // #endif

}
